# Databricks notebook source
## Imports

import json
from pyspark.sql.types import StringType, DecimalType, DoubleType, IntegerType, LongType, BinaryType, DateType
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField
from pyspark.sql.functions import expr

class GlueRequestStream:
  def __init__(self, sFolderPath, oCloudfilesBaseConfig):
    self._sFolderPath = sFolderPath
    
    
    oCloudfilesAdditionalConfig = {
      'cloudFiles.format': 'text',
      'cloudFiles.useNotifications': False,
      'cloudFiles.maxFilesPerTrigger': 1,
      'cloudFiles.validateOptions': False
    }
    
    
    self._df = (spark.readStream.format("cloudfiles")
                .options(**{**oCloudfilesBaseConfig, **oCloudfilesAdditionalConfig})
                .load(self._sFolderPath + '/*_m.json'))
  
    
  def forEach(self, fFunction):
    sPath = self._sFolderPath
    
    def test(microDf, batchId):
      #s = microDf.drop(df.tail(1).index,inplace=True)
      #oRequest = GlueRequest('abfss://sapas1@storgluecontent.dfs.core.windows.net/RAW/SFLIGHT', s)
      #oDeltaTable =   DeltaTableForGlue('abfss://sapas1@storgluecontent.dfs.core.windows.net/CLEANSED/SFLIGHT2').merge(oRequest)
      #display(microdf)
      #microDf.write.format("csv").save('abfss://sapas1@storgluecontent.dfs.core.windows.net/CLEANSED/test.csv')
      
      aRows = microDf.collect()
      sJsonString = '['

      for row in aRows:
        sJsonString = sJsonString + row['value']


      sJsonString = sJsonString.replace('}{','},{')
      sJsonString = sJsonString + ']'
      
      aRequests =  json.loads(sJsonString)
      
      for request in aRequests:
        oRequest = GlueRequest(sPath, request)
        fFunction(oRequest)
        

    
    return self._df.writeStream.foreachBatch(test).option("checkpointLocation", sPath + '/_autostream_chk')
    

class GlueRequest:
    def __init__(self, *args):
      
      self._aFiles = []
      self.dfFields = {}
      self.schema = {}
      self.dfData = {}
     
        # sum of args 
      if len(args) == 1: 
        self._sFullPathToJson = args[0]
        self._sFileName = self._sFullPathToJson.split('/').pop(-1)
        self._sFolderPath = self._sFullPathToJson.replace('/'+self._sFileName, '')
        self._loadMetadataFromPath(self._sFullPathToJson)
        
      if len(args) == 2:
        self._sFolderPath = args[0]
        self._loadMetadataFromObject(args[1])
        
      
    def _readJsonFile(self, sPath):
      oDf = spark.read.load(sPath, format='text')
      
      aRows = oDf.collect()
      sJsonString = ''

      for row in aRows:
        sJsonString = sJsonString + row['value']

      return json.loads(sJsonString)
    

    def _loadMetadataFromPath(self, sFullPathToJson):

        ## read GLUE request "handshake" file (*_m.json)
        
        self._metadata = self._readJsonFile(sFullPathToJson)['GlueRequest']
        
        return self._extractCSVpaths()
      
    def _loadMetadataFromObject(self, oJsonObj):

        ## read GLUE request "handshake" file (*_m.json)
        
        self._metadata = oJsonObj['GlueRequest']
        
        return self._extractCSVpaths()
      
    def _extractCSVpaths(self):
      
        ## get csv files (and remove duplicates)
        if int(self.getMetadata("LinesTransferred")) > 0:
          self._aFiles = list(dict.fromkeys(self._metadata['Data']))
          for i in range(len(self._aFiles)):
            self._aFiles[i] = self._sFolderPath +'/'+ self._aFiles[i].split('/').pop(-1)
        return self
    
    def getFolderPath(self):
        return self._sFolderPath

    def getMetadata(self, sKey="ALL"):
        if self._metadata == {} :
            self._loadMetadata()
        
        if(sKey == "ALL"):
            return self._metadata
        else:
            return self._metadata[sKey]
        

    def _loadFields(self):
        ## read Glue Table metadata json file and create DF

        self._oSchemaMetadata = self._readJsonFile(self._sFolderPath + '/*_s.json')
        df_sapFields = spark.createDataFrame(self._oSchemaMetadata['Fields'])


        ## type mapping (SAP -> Spark)

       
        ## create type mapping DF 
        sapToSpark = [
            ('ACCP', 'StringType'),
            ('CHAR', 'StringType'),
            ('CLNT', 'StringType'),
            ('CUKY', 'StringType'),
            ('CURR', 'DecimalType'),
            #('DATS', 'DateType'), #DATS will be converted later
            ('DATS', 'StringType'),
            ('DEC', 'DecimalType'),
            ('FLTP', 'DoubleType'),
            ('INT1', 'IntegerType'),
            ('INT2', 'IntegerType'),
            ('INT4', 'LongType'),
            ('LANG', 'StringType'),
            ('LCHR', 'StringType'),
            ('LRAW', 'StringType'),
            ('NUMC', 'StringType'),
            ('PREC', 'IntegerType'),
            ('QUAN', 'DecimalType'),
            ('RAW', 'BinaryType'),
            ('RSTR', 'StringType'),
            ('SSTR', 'StringType'),
            ('STRG', 'StringType'),
            ('TIMS', 'StringType'),
            ('UNIT', 'StringType'),
            ('VARC', 'StringType'),
            ('D34D', 'DoubleType')
        ]
        sapToSparkColumns = ["SAPDataType","SparkType"]
        df_sapToSpark = spark.createDataFrame(data=sapToSpark, schema = sapToSparkColumns)

        ## join metadata and type mapping
        df_fieldInfo = df_sapFields.join(df_sapToSpark, df_sapFields.DataType == df_sapToSpark.SAPDataType,how='left_outer')

        self.dfFields = df_fieldInfo.withColumn('FieldName', regexp_replace('FieldName', '/', '')) #remove / from fieldnames ... BW...
        

        return self
    
    def getFields(self):
        if self.dfFields == {} :
            self._loadFields()
            
        return self.dfFields
    
    def _getSchema(self):

        if self.schema == {}:

            a_typeMapping = self.getFields().select("FieldName", "SparkType", "Length", "Decimals", "Position").sort("Position").collect()

            ## create new schema with types

            

            ## create new schema with spark types
            a_fields = []
            for field in a_typeMapping :
                
                if field['SparkType'] == 'DecimalType' :
                    a_fields.append(StructField(field['FieldName'], globals()[field['SparkType']](int(field['Length']), int(field['Decimals'])), True))
                ##elif field['SparkType'] == 'DateType' :
                ##    fields.append(StructField(field['FieldName'], globals()[field['SparkType']](), True))
                else :   
                    a_fields.append(StructField(field['FieldName'], globals()[field['SparkType']](), True))

            self.schema = StructType(a_fields) 

        return self.schema

    def getData(self):

        self._sFieldNameGlueRequest = self.getFields().select('FieldName').where('RollName = "/DVD/GL_EXT_DATA_REQUEST"').collect()[0]['FieldName']
         #if len(self._aFiles) > 0
        ## read csv data with new schema

        df_data = spark.read.load(
            self._aFiles, 
            format='csv', 
            delimiter=self.getMetadata('Delimiter'), 
            header=True, ## known issue, we need a Flag!!!
            schema = self._getSchema())

        

        df_data = df_data.drop(self._sFieldNameGlueRequest)

        ## convert dates yyyyMMdd for all columns with 'DATS'

        

        for col in self.getFields().select("FieldName").where("DataType = 'DATS'").collect() :
            df_data = df_data.withColumn(col.FieldName, expr("to_date("+ col.FieldName +", 'yyyyMMdd')"))
            
            
        self.dfData = df_data
        return self.dfData




## GlueDeltaTable

from delta.tables import *

class DeltaTableForGlue:
    def __init__(self, sFullPathToDeltaTable):

        
      self._sFullPathToDeltaTable = sFullPathToDeltaTable
      self._createDeltaTable()

    def _createDeltaTable(self):
        try:
            self.oDt = DeltaTable.forPath(spark, self._sFullPathToDeltaTable)
        except:
            self.oDt = {}
            #print("fail")
    
    def getData(self):

        if self.oDt == {}:
            self._createDeltaTable()
        
        try:
            return self.oDt.toDF()
        except:
            "no delta table"

    def merge(self, oGlueRequest):
        ## write parquet to target layer 

        ## (!!! overwrite all !! TBD?!)


        df_data = oGlueRequest.getData()
        df_fieldInfo = oGlueRequest.getFields()

        s_fieldNameGlueRequest = df_fieldInfo.select('FieldName').where('RollName = "/DVD/GL_EXT_DATA_REQUEST"').collect()[0]['FieldName']

        aPartitionColumns = []
        for field in oGlueRequest.getFields().select('FieldName').where('Partitioning == "X" AND RollName != "/DVD/GL_EXT_DATA_REQUEST"').orderBy('Position').collect():
            aPartitionColumns.append(field['FieldName'])

        if oGlueRequest.getMetadata('DeltaType') == 'FULL' :

            if 'ODQ_CHANGEMODE' in df_data.columns:
                df_data = df_data.drop('ODQ_CHANGEMODE')
        
            # overwrite or create delta
            df_data.write.format("delta").mode("overwrite").partitionBy(aPartitionColumns).save(self._sFullPathToDeltaTable)
            self._createDeltaTable()

        elif oGlueRequest.getMetadata('DeltaType') == 'TRIGGER' :

            s_fieldNameDelFlag = df_fieldInfo.select('FieldName').where('RollName = "/DVD/GL_DELFLAG"').collect()[0]['FieldName']

            if oGlueRequest.getMetadata('LoadType') == 'L' :

                df_data = df_data.drop(s_fieldNameDelFlag)
            
                # overwrite or create delta
                df_data.write.format("delta").mode("overwrite").partitionBy(aPartitionColumns).save(self._sFullPathToDeltaTable)

            elif oGlueRequest.getMetadata('LoadType') == 'D' :

                self._createDeltaTable()


                a_conditions = [] # keys has to match ...
                for col in df_fieldInfo.select('FieldName').where('KeyFlag == "X"').collect():
                    a_conditions.append('source.'+ col['FieldName'] +' = target.'+ col['FieldName'])

                o_updateSet = {} # fields/cols to update -> no character fields
                for col in df_fieldInfo.select('FieldName').where('KeyFlag == "" AND Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameDelFlag + '"').collect():
                    o_updateSet[col['FieldName']] = 'source.'+ col['FieldName']

                o_insertValues = {} # all fields/cols without technical ones from glue
                for col in df_fieldInfo.select('FieldName').where('Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameDelFlag + '"').collect():
                    o_insertValues[col['FieldName']] = 'source.'+ col['FieldName']

                # merge magic ;)
                self.oDt.alias("target").merge(
                    df_data.alias("source"), ' AND '.join(a_conditions)) \
                    .whenMatchedDelete(condition = s_fieldNameDelFlag+' = "D"') \
                    .whenMatchedUpdate(condition = s_fieldNameDelFlag+' IS NULL', set = o_updateSet ) \
                    .whenNotMatchedInsert(condition = s_fieldNameDelFlag+' IS NULL', values = o_insertValues) \
                    .execute()

        elif oGlueRequest.getMetadata('DeltaType') == 'REQUEST' :

            s_fieldNameRecordMode = df_fieldInfo.select('FieldName').where('RollName = "RODMUPDMOD"').collect()[0]['FieldName']

            if oGlueRequest.getMetadata('LoadType') == 'L' :

                df_data = df_data.drop(s_fieldNameRecordMode)
            
                # overwrite or create delta
                df_data.write.format("delta").mode("overwrite").save(self._sFullPathToDeltaTable)
                self._createDeltaTable()

            elif oGlueRequest.getMetadata('LoadType') == 'D' :

                self._createDeltaTable()

                a_conditions = [] # keys has to match ...
                for col in df_fieldInfo.select('FieldName').where('KeyFlag == "X"').collect():
                    a_conditions.append('source.'+ col['FieldName'] +' = target.'+ col['FieldName'])

                o_updateSet = {} # fields/cols to update -> no character fields
                for col in df_fieldInfo.select('FieldName').where('KeyFlag == "" AND Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameRecordMode + '"').collect():
                    o_updateSet[col['FieldName']] = 'source.'+ col['FieldName']

                o_insertValues = {} # all fields/cols without technical ones from glue
                for col in df_fieldInfo.select('FieldName').where('Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameRecordMode + '"').collect():
                    o_insertValues[col['FieldName']] = 'source.'+ col['FieldName']

                # merge magic ;)
                self.oDt.alias("target").merge(
                    df_data.alias("source"), ' AND '.join(a_conditions)) \
                    .whenMatchedDelete(condition = s_fieldNameRecordMode+' = "D" OR '+s_fieldNameRecordMode+' = "R"') \
                    .whenMatchedUpdate(condition = s_fieldNameRecordMode+' IS NULL', set = o_updateSet ) \
                    .whenNotMatchedInsert(condition = s_fieldNameRecordMode+' = "N"', values = o_insertValues) \
                    .execute()
                
        elif oGlueRequest.getMetadata('DeltaType') == 'ODQ' :

          s_fieldNameODQ_CHANGEMODE = df_fieldInfo.select('FieldName').where('RollName = "ODQ_CHANGEMODE"').collect()[0]['FieldName']

          if oGlueRequest.getMetadata('LoadType') == 'F' :

              df_data = df_data.drop(s_fieldNameODQ_CHANGEMODE)

              # overwrite or create delta
              df_data.write.format("delta").mode("overwrite").save(self._sFullPathToDeltaTable)
              self._createDeltaTable()

          elif oGlueRequest.getMetadata('LoadType') == 'D' :

              self._createDeltaTable()

              a_conditions = [] # keys has to match ...
              for col in df_fieldInfo.select('FieldName').where('KeyFlag == "X"').collect():
                  a_conditions.append('source.'+ col['FieldName'] +' = target.'+ col['FieldName'])

              o_updateSet = {} # fields/cols to update -> no character fields
              for col in df_fieldInfo.select('FieldName').where('KeyFlag == "" AND Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameODQ_CHANGEMODE + '"').collect():
                  o_updateSet[col['FieldName']] = 'source.'+ col['FieldName']

              o_insertValues = {} # all fields/cols without technical ones from glue
              for col in df_fieldInfo.select('FieldName').where('Fieldname != "'+ s_fieldNameGlueRequest +'" AND Fieldname != "'+ s_fieldNameODQ_CHANGEMODE + '"').collect():
                  o_insertValues[col['FieldName']] = 'source.'+ col['FieldName']

              # merge magic ;)
              self.oDt.alias("target").merge(
                  df_data.alias("source"), ' AND '.join(a_conditions)) \
                  .whenMatchedDelete(condition = s_fieldNameODQ_CHANGEMODE+' = "D"') \
                  .whenMatchedUpdate(condition = s_fieldNameODQ_CHANGEMODE+' = "U"', set = o_updateSet ) \
                  .whenNotMatchedInsert(condition = s_fieldNameODQ_CHANGEMODE+' = "C"', values = o_insertValues) \
                  .execute()

        self._createDeltaTable()
        #self.oDt.generate("symlink_format_manifest")

        print(oGlueRequest.getMetadata('JobName'), ' [',oGlueRequest.getMetadata('Request'), '] (' +oGlueRequest.getMetadata('UserName'), ')')
        print('   read:     ' + oGlueRequest.getMetadata('LinesRead'))
        print('   written:  ' + oGlueRequest.getMetadata('LinesTransferred'))
        print()
        print('Delta operation: ' + self.oDt.history(1).select('operation').collect()[0]['operation'])
        print(self.oDt.history(1).select("operationMetrics").collect()[0]['operationMetrics'])

        return self




    def registerAsExternalTable(self, sTableName):
         
        sCommand = "CREATE TABLE IF NOT EXISTS {sTableName} USING DELTA LOCATION '{sFullPathToDeltaTable}'"

        # sCommand = "create VIEW {sTableName} as select * from (select *, SUBSTRING( rows.filepath() , LEN(rows.filepath()) -  CHARINDEX('/',REVERSE(rows.filepath())) + 2  , LEN(rows.filepath())  )  filename "\
        #     "from openrowset("\
        #         "bulk '{sFullPathToDeltaTable}/*.parquet',"\
        #         "format = 'parquet') as rows) as deltatbl where filename in "\
        #         "(select SUBSTRING( C1 , LEN(C1) -  CHARINDEX('/',REVERSE(C1)) + 2  , LEN(C1)  )"\
        #     "from openrowset("\
        #         "bulk '{sFullPathToDeltaTable}/_symlink_format_manifest/manifest',"\
        #         "format = 'csv',"\
        #         "parser_version = '2.0',"\
        #         "firstrow = 1) as filelist)"

        sCommand = sCommand.replace('{sTableName}', sTableName)
        sCommand = sCommand.replace('{sFullPathToDeltaTable}', self._sFullPathToDeltaTable)
        #print(sCommand)
        spark.sql(sCommand)
                # https://www.youtube.com/watch?v=7ewmcdrylsA
                # https://docs.microsoft.com/de-de/azure/databricks/delta/delta-update

                # https://wiki.scn.sap.com/wiki/display/EIM/Building+Dataflows+with+Extractors

                # https://help.sap.com/saphelp_nw73/helpdata/en/e2/018a535af43d58e10000000a174cb4/content.htm?no_cache=true

    def registerAsExternalTable2(self, sTableName):

        #todo: adapt file path for partitioning: means add the numer of partitions with '*/' to string ....
         
        #sCommand = "CREATE TABLE IF NOT EXISTS {sTableName} USING DELTA LOCATION '{sFullPathToDeltaTable}'"

        sCommand = "create VIEW {sTableName} as select * from (select *, SUBSTRING( rows.filepath() , LEN(rows.filepath()) -  CHARINDEX('/',REVERSE(rows.filepath())) + 2  , LEN(rows.filepath())  )  filename "\
            "from openrowset("\
                "bulk '{sFullPathToDeltaTable}/*.parquet',"\
                "format = 'parquet') as rows) as deltatbl where filename in "\
                "(select SUBSTRING( C1 , LEN(C1) -  CHARINDEX('/',REVERSE(C1)) + 2  , LEN(C1)  )"\
            "from openrowset("\
                "bulk '{sFullPathToDeltaTable}/_symlink_format_manifest/manifest',"\
                "format = 'csv',"\
                "parser_version = '2.0',"\
                "firstrow = 1) as filelist)"

        sCommand = sCommand.replace('{sTableName}', sTableName)
        sCommand = sCommand.replace('{sFullPathToDeltaTable}', self._sFullPathToDeltaTable)
        print(sCommand)
        #spark.sql(sCommand)
                # https://www.youtube.com/watch?v=7ewmcdrylsA
                # https://docs.microsoft.com/de-de/azure/databricks/delta/delta-update

                # https://wiki.scn.sap.com/wiki/display/EIM/Building+Dataflows+with+Extractors

                # https://help.sap.com/saphelp_nw73/helpdata/en/e2/018a535af43d58e10000000a174cb4/content.htm?no_cache=true



        
class AzureStorageFile:
    def __init__(self, sType):

        self._sSasToken = ""

        if sType == 'dfs':
            self._bDfs = True
            self._bBlob = False
            self._sType = sType
        elif sType == 'blob':
            self._bDfs = False
            self._bBlob = True
            self._sType = sType
        else:
            print("excp has to be thrown")

    def withSasToken(self, sToken):
        self._sSasToken = sToken

        return self
    
    def _createFullPath(self, sContainerName, sStorageAccountName, sSubPath):

        if self._sSasToken != '':
            spark.conf.set("fs.azure.sas."+ sContainerName +"."+ sStorageAccountName +"."+ self._sType +".core.windows.net", self._sSasToken)
            spark.conf.set("fs.azure.account.key."+ sStorageAccountName +"."+ self._sType +".core.windows.net", self._sSasToken)

        if self._bDfs:
            self.sPath = 'abfss://'+ sContainerName +'@'+ sStorageAccountName +'.dfs.core.windows.net/' + sSubPath

        if self._bBlob :
            self.sPath = 'wasbs://'+ sContainerName +'@'+ sStorageAccountName +'.blob.core.windows.net/' + sSubPath
            #workaround for connecting to blob storage via sas token; read first with spark read than connection auth is established and sc.wholeTextFiles works aswell
            #df_data = spark.read.load(self.sPath, format='csv')


        return self.sPath

    def byPath(self, sPath):
        
        sPathWithoutProtocol = sPath.split('://').pop(-1)
        sStorageAccountName = sPathWithoutProtocol.split('.').pop(0)
        aRelativePathSegments = sPathWithoutProtocol.split('.windows.net/').pop(-1).split('/')
        sContainerName = aRelativePathSegments.pop(0)
        sSubPath = '/'.join(aRelativePathSegments)
        
        return self._createFullPath(sContainerName, sStorageAccountName, sSubPath)

    def byParams(self, sFolderPath, sFileName, sStorageAccountName):

        
        aRelativePathSegments = sFolderPath.split('/')
        aRelativePathSegments.append(sFileName)
        sContainerName = aRelativePathSegments.pop(0)
        sSubPath = '/'.join(aRelativePathSegments)

        return self._createFullPath(sContainerName, sStorageAccountName, sSubPath)

    
        
    ## Test
    #AzureStorageFile('dfs').byPath("https://dtaa001itweud01dapsts01.blob.core.windows.net/sap-bhz/ZADLS_ABX_SKP_CMV04_0009/ZADLS_ABX_SKP_CMV04_0009_18.json")

    #s_folderPath = 'sap-bhz/ZADLS_ZPMCOVP09' #dynamically from trigger
    #s_fileName = 'ZADLS_ZPMCOVP09_381.json' #dynamically from trigger
    #s_storageAccountName = 'dtaa001itweud01dapsts01' #defined in pipeline

    #AzureStorageFile('dfs').withSasToken("dawdawdawdadw").byParams(s_folderPath, s_fileName, s_storageAccountName)

# COMMAND ----------

# Config
AzureStorageFile('dfs').withSasToken("MH1Y5UDp70PSCLA8HS6Sdv4ln9rqWvWaem8511pUjLKAfoG393zIjJsXm5mjQCmW0uczZsibBN4yIh0Vmvu0Uw==").byPath("https://storgluecontent.blob.core.windows.net/sapas1/RAW/SFLIGHT")
oCloudConfig = {
  'cloudFiles.subscriptionId': '95c11231-f9c7-4799-9beb-267ecfab2454',
  'cloudFiles.tenantId': '6fdc3117-ec29-4d73-8b33-028c8c300872',
  'cloudFiles.clientId': '51743729-b6c1-4f87-a212-93bbe7476d0c',
  'cloudFiles.clientSecret': 'dcx_su96SP2vN3j~sTHHBHw_h11lm_a.o0',
  'cloudFiles.resourceGroup': 'rg-glue-content-dev',
  'cloudFiles.connectionString': 'BlobEndpoint=https://storgluecontent.blob.core.windows.net/;QueueEndpoint=https://storgluecontent.queue.core.windows.net/;FileEndpoint=https://storgluecontent.file.core.windows.net/;TableEndpoint=https://storgluecontent.table.core.windows.net/;SharedAccessSignature=sv=2020-02-10&ss=q&srt=sco&sp=rwdlacup&se=2099-03-18T18:05:54Z&st=2021-03-18T10:05:54Z&spr=https&sig=jXHs%2FpjV40IkqYmrlSzweCNT61VDa3ecYuZcVN1r4gU%3D',
}

# COMMAND ----------

# DBTITLE 1,Streaming Demo
def merge(oRequest):
  DeltaTableForGlue('abfss://sapas1@storgluecontent.dfs.core.windows.net/CLEANSED/SFLIGHT2').merge(oRequest)
  
GlueRequestStream('abfss://sapas1@storgluecontent.dfs.core.windows.net/RAW/SFLIGHT', oCloudConfig).forEach(merge).start()
#trigger(once=True)

# COMMAND ----------

# DBTITLE 1,Simple Lib Demo
oRequest = GlueRequest(AzureStorageFile('dfs').withSasToken("MH1Y5UDp70PSCLA8HS6Sdv4ln9rqWvWaem8511pUjLKAfoG393zIjJsXm5mjQCmW0uczZsibBN4yIh0Vmvu0Uw==").byPath("https://storgluecontent.blob.core.windows.net/sapas1/RAW/SFLIGHT/ZAZR_DT_SFLIGHT_T_1774_m.json"))
DeltaTableForGlue('abfss://sapas1@storgluecontent.dfs.core.windows.net/CLEANSED/SFLIGHT').merge(oRequest)

#oRequest = GlueRequest('pathToHandshakeJsonFile')
#    .getMetadata('empty or key') -> returns request metadata (handshake json)
#    .getFields() -> returns a DataFrame with all Fields (structure json)
#    .getData() -> returns a DataFrame which holds data (csv) - types converted

#oDeltaTable = DeltaTableForGlue('pathToDeltaFolder')
#    .merge('GlueRequestObject') -> automatic merge by delta flag/ recordmode

# COMMAND ----------

oRequest = GlueRequest(AzureStorageFile('dfs').withSasToken("eRxmlX34gLu/y8Y/4gpmTXL6UJAF6KufFa1W14lbbFELu0/tL9kMBbOfvq+/PWhaYSvDB7XieVWyMrPous4KjQ==").byPath("https://lakedvd.blob.core.windows.net/s4d/raw/FLIGHTSCDS/ZCD_FLIGHTSCDS_111.json"))

# COMMAND ----------

oDF = oRequest.getData()

# COMMAND ----------

oDF.select('*').count()

# COMMAND ----------

display(oRequest.getData());

# COMMAND ----------

oRequest = GlueRequest(AzureStorageFile('dfs').withSasToken("eRxmlX34gLu/y8Y/4gpmTXL6UJAF6KufFa1W14lbbFELu0/tL9kMBbOfvq+/PWhaYSvDB7XieVWyMrPous4KjQ==").byPath("https://lakedvd.blob.core.windows.net/s4d/raw/FLIGHTSCDS/ZCD_FLIGHTSCDS_111.json"))

oRequest.getMetadata()

oRequest._metadata['DeltaType'] = 'ODQ'
oRequest._metadata['LoadType'] = 'D' # F/D

DeltaTableForGlue('abfss://s4d@lakedvd.dfs.core.windows.net/cleansed/FLIGHTSCDS').merge(oRequest)

